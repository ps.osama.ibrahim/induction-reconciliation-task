var activeTab = "MatchingTransactions";
let step = 0;
let target = $("#contentArea");
let part1 = $("#part1");
let part2 = $("#part2");
let part3 = $("#part3");
let btnPrevious = $(".btnPrevious");
let btnNext = $(".btnNext");
let btnSubmit = $(".btnSubmit");
let preSendInfo = {};
var tempFile = "";

renderButtons();

$(".custom-file-input").on("change", function() {
  var fileName = $(this)
    .val()
    .split("\\")
    .pop();
  $(this)
    .siblings(".custom-file-label")
    .addClass("selected")
    .html(fileName);
});

function onSubmit() {
  var form = $("#filesForm")[0];
  var formData = new FormData(form);
  let data1 = {
    file1: $("#file1").prop("files"),
    file2: $("#file2").prop("files"),
    type1: $("#type1")
      .children("option:selected")
      .val(),
    type2: $("#type2")
      .children("option:selected")
      .val()
  };
  // TODO you should have the urLs set dynamically - Done
  $.ajax({
    url: siteBase + "reconciliation",
    data: formData,
    method: "POST",
    contentType: false,
    processData: false,
    beforeSend: function() {
      renderResult();
    },
    success: function(d) {
      tempFile = d.tempFile;
      getResultData(
         siteBase +"static?file=MatchingTransactions&ext=c"
      );
    },
    error: function(err) {
      alert("Invalid files, make sure you upload and validate your files");
      console.log(err);
    }
  });
}

function changeStep(e, num) {
  $(".part1alert").hide();
  $(".part2alert").hide();
  $.each(document.getElementsByClassName("inpFileUpload"), function() {
    $(this).removeClass("is-invalid");
  });

  if (step == 0) {
    if ($("#file1").prop("files").length == 0) {
      $("#file1").addClass("is-invalid");
      $(".part1alert").show();
      $(".part1alert").html("Please Upload a file");
      return;
    } else {
      let ddl1 = $("#type1").val();
      let aux = $("#file1")
        .val()
        .split(".");
      let ext = aux[aux.length - 1].toUpperCase();
      if (ext != ddl1.toUpperCase()) {
        $(".part1alert").show();
        $(".part1alert").html("Invalid upload file");
        return;
      }
    }
  }
  if (step == 1) {
    if ($("#file2").prop("files").length == 0) {
      $("#file2").addClass("is-invalid");
      $(".part2alert").show();
      $(".part2alert").html("Please Upload a file");
      return;
    } else {
      let ddl2 = $("#type2").val();
      let aux = $("#file2")
        .val()
        .split(".");
      let ext = aux[aux.length - 1].toUpperCase();
      if (ext != ddl2.toUpperCase()) {
        $(".part2alert").show();
        $(".part2alert").html("Invalid upload file");
        return;
      }
    }
  }

  step += num;
  if (step > 2) step = 2;
  else if (step < 0) step = 0;
  if (step == 2) {
    $("#spanName1").text($("#name1").val());
    $("#spanName2").text($("#name2").val());

    $("#spaType1").text(
      $("#type1")
        .children("option:selected")
        .val()
    );
    $("#spaType2").text(
      $("#type2")
        .children("option:selected")
        .val()
    );
  }
  renderButtons();
}

function renderButtons() {
  if (step == 0) {
    part1.show();
    btnNext.show();
    part2.hide();
    part3.hide();
    btnPrevious.hide();
    btnSubmit.hide();
  }

  if (step == 1) {
    part1.hide();
    part2.show();
    part3.hide();
    btnPrevious.show();
    btnNext.show();
    btnSubmit.hide();
  }

  if (step == 2) {
    part1.hide();
    part2.hide();
    part3.show();
    btnPrevious.show();
    btnNext.hide();
    btnSubmit.show();
  }
}

function renderResult() {
  target.html(`
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link tab1 active" href="#">Matched</a>
            </li>
            <li class="nav-item">
              <a class="nav-link tab2" href="#">Mismatching</a>
            </li>
            <li class="nav-item">
              <a class="nav-link tab3" href="#">Missing</a>
            </li>
          </ul>
          <div id="resultTable"></div>
          <div>
          <a href="/reconciliation" class="btn btn-primary">Compare new Files</a>
            <select class="form-control" id="dlType" name="dlType" onchange="onDownloadChange(this)">
                  <option value="dl">Download</option>
                  <option value="c">CSV</option>
                  <option value="j">JSON</option>
              </select>
          </div>
          `);
}
$(document).ready(function() {
  $(".part1alert").hide();
  $(".part2alert").hide();
  $("body").on("click", ".nav-link", function() {
    $(".nav-link").removeClass("active");
    $(this).addClass("active");
  });

  $("body").on("click", ".tab1", function() {
    getResultData(
       siteBase +"static?file=MatchingTransactions&ext=c"
    );
    activeTab = "MatchingTransactions";
    $("#dlType").prop("selectedIndex", 0);
  });

  $("body").on("click", ".tab2", function() {
    activeTab = "MismatchingTransactions";
    getResultData(
       siteBase +"static?file=MismatchingTransactions&ext=c"
    );
    $("#dlType").prop("selectedIndex", 0);
  });

  $("body").on("click", ".tab3", function() {
    activeTab = "MissingTransactions";
    getResultData(
       siteBase +"static?file=MissingTransactions&ext=c"
    );
    $("#dlType").prop("selectedIndex", 0);
  });
});

function onDownloadChange(e) {
  if (e.value == "dl") return;
  let uri =
     siteBase + "static?tempFolder=" +
    tempFile +
    "&file=" +
    activeTab +
    "&ext=" +
    e.value;
  window.location.href = uri;
}

function getResultData(url) {
  $.ajax({
    url,
    method: "GET",
    success: function(data) {
      var result_data = data.split(/\r?\n|\r/);
      var table_data = '<table class="table table-bordered table-striped">';
      var c = 0;
      for (var count = 0; count < result_data.length; count++) {
        var cell_data = result_data[count].split(",");

        if (cell_data.length <= 1) break;

        if (c == 0) {
          cell_data.unshift("#");
        } else {
          cell_data.unshift(c);
        }
        c++;

        table_data += "<tr>";
        counter = 1;
        for (var cell_count = 0; cell_count < cell_data.length; cell_count++) {
          if (count === 0) {
            table_data += "<th>" + cell_data[cell_count] + "</th>";
          } else {
            table_data += "<td>" + cell_data[cell_count] + "</td>";
          }
        }
        table_data += "</tr>";
      }
      table_data += "</table>";
      $("#resultTable").html(table_data);
    },
    error: function(err) {
      console.log(err);
      alert("Unable to process event, please try later");
    }
  });
}
