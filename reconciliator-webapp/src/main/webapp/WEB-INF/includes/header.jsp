<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url var="jqueryUrl" value="/jquery/jquery-3.4.1.js" />

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${jqueryUrl}"></script>

    <title>Converter</title>
    <script type="text/javascript">
        siteBase = '${pageContext.request.contextPath}/';
    </script>
</head>
<body>
<div class="container mt-5">
