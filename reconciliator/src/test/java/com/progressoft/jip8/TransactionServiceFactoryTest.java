package com.progressoft.jip8;

import com.progressoft.jip8.CSVTransactionServiceProvider;
import com.progressoft.jip8.InvalidFileException;
import com.progressoft.jip8.JSONTransactionServiceProvider;
import com.progressoft.jip8.TransactionServiceFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TransactionServiceFactoryTest {
    @Test
    public void giveValidExtension_whenInvokingFactory_ReturnValidObject(){
        Assertions.assertEquals(JSONTransactionServiceProvider.class ,
                TransactionServiceFactory.getTransactionService("src/test/resources/test.json","json").getClass());

        Assertions.assertEquals(CSVTransactionServiceProvider.class ,
                TransactionServiceFactory.getTransactionService("src/test/resources/test.csv","csv").getClass());
    }
    @Test
    public void givenInvalidExtension_whenInvokingFactory_ReturnValidObject(){
      Exception e = Assertions.assertThrows(IllegalArgumentException.class,()->
               TransactionServiceFactory.getTransactionService("src/test/resources/test.txt","txt")
               );
      Assertions.assertEquals("Unknown file type",e.getMessage());
    }
    @Test
    public void givenMismatchingExtensionandFile_whenInvokingFactory_throwException(){
        Exception e = Assertions.assertThrows(InvalidFileException.class,()->
                TransactionServiceFactory.getTransactionService("src/test/resources/test.csv","json")
        );
    }
}
