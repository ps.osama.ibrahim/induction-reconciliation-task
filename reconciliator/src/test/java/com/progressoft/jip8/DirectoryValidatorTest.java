package com.progressoft.jip8;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DirectoryValidatorTest {
    @Test
    @Disabled("bad scenario")
    public void givenInvalidDirectory_whenValidated_createPath() {
        // TODO it do create not valdiate - Done, fixed naming and made the values abstract of system type or path
        String path = System.getProperty("user.home") + File.separator+ "testPath";
        DirectoryValidator.createPathIfNotExist(path);
        Assertions.assertEquals(true, Files.exists(Paths.get(path)));
    }

    @Test
    public void givenInvlidDiretory_whenCreating_created() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            DirectoryValidator.createPathIfNotExist("/home/testPath");
        });
    }
}