package com.progressoft.jip8;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class ComparitorTest {
    // TODO this should be moved to a setup function or inside the test case - Done
    private static String source;
    private static String sourceExtension;
    private static String target;
    private static String targetExtension;
    private static String path;

    @BeforeAll
    private static void init() {
        source = "src/test/resources/in.csv";
        sourceExtension = "csv";
        target = "src/test/resources/in.json";
        targetExtension = "json";
        path = System.getProperty("user.home") + "/reconciliation-results/";
    }

    @Test
    public void givenValidInputs_whenConciliating_Pass() {
        TransactionService sourceService = TransactionServiceFactory.getTransactionService(source, sourceExtension);
        TransactionService targetService = TransactionServiceFactory.getTransactionService(target, targetExtension);
        Comparitor comparitor = new Comparitor(sourceService, targetService, path);
        comparitor.compare();
    }
}
