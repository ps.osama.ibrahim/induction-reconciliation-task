package com.progressoft.jip8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {

            System.out.println(">> Enter source file location: ");
            String source = scanner.nextLine().trim();
            System.out.println(">> Enter source file format: ");
            String sourceExtension = scanner.nextLine().trim();
            System.out.println(">> Enter target file location: ");
            String target = scanner.nextLine().trim();
            System.out.println(">> Enter target file format: ");
            String targetExtension = scanner.nextLine().trim();

            TransactionService sourceService = TransactionServiceFactory.getTransactionService(source, sourceExtension);
            TransactionService targetService = TransactionServiceFactory.getTransactionService(target, targetExtension);

            String path = System.getProperty("user.home") + "/reconciliation-results/";
            Comparitor comparitor = new Comparitor(sourceService, targetService, path);
            comparitor.compare();
            System.out.println("Reconciliation finished");
            System.out.println("Result files are available in directory " + path);
        }
    }
}
