package com.progressoft.jip8;

import java.nio.file.Paths;

public class TransactionServiceFactory {
    public static TransactionService getTransactionService(String path, String extension){
        switch(extension.toLowerCase()){
            case "json": return new JSONTransactionServiceProvider(Paths.get(path));
            case "csv": return new CSVTransactionServiceProvider(Paths.get(path));
            default: throw new IllegalArgumentException("Unknown file type");
        }
    }

}
//Check service loader
