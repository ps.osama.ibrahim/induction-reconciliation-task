package com.progressoft.jip8;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DirectoryValidator {
    public static void createPathIfNotExist(String path) {
        File file = new File(path);
        if (!file.exists()) {
            createPath(file);
        }
    }

    private static void createPath(File file) {
        // TODO use Paths and Files - Done
        Path path = Paths.get(file.getPath());
        if(!Files.exists(path)){
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                throw new IllegalArgumentException("Directory can't be created");
            }
        }
    }
}
