package com.progressoft.jip8;

import java.util.Map;

public class Comparitor {

    private Map<String, FinancialTransaction> sourceList;
    private Map<String, FinancialTransaction> targetList;
    private Filewriter matchingWriter;
    private Filewriter mismatchWriter;
    private Filewriter missingWriter;


    public Comparitor(TransactionService source, TransactionService target, String outputPath) {
        validateInputs(source, target, outputPath);
        validateOutputPath(outputPath);
        // TODO all logic inside this constructor should go in the method - Done
        setupVariables(source, target, outputPath);
        prepareFiles();

    }

    private void setupVariables(TransactionService source, TransactionService target, String outputPath) {
        this.sourceList = source.getAllRecords();
        this.targetList = target.getAllRecords();
        this.matchingWriter = new CSVFileWriter(outputPath, "MatchingTransactions.csv");
        this.mismatchWriter = new CSVFileWriter(outputPath, "MismatchingTransactions.csv");
        this.missingWriter = new CSVFileWriter(outputPath, "MissingTransactions.csv");
    }

    public void compare() {
        sourceList.forEach(this::findMatch);
        targetList.forEach(this::handleRemainingTarget);
    }

    private void handleRemainingTarget(String k, FinancialTransaction t) {
        missingWriter.write(t.toCSVRecord("TARGET"), true);
    }

    private void findMatch(String key, FinancialTransaction sourceValue) {
        Transaction targetValue = targetList.get(key);
        if (targetValue == null) {
            missingWriter.write(sourceValue.toCSVRecord("SOURCE"), true);
            return;
        }
        match(key, sourceValue, targetValue);
    }

    private void match(String transactionKey, Transaction sourceValue, Transaction targetValue) {
        targetList.remove(transactionKey);
        if (targetValue.equals(sourceValue)) {
            matchingWriter.write(sourceValue.toCSVRecord(), true);
            return;
        }
        mismatchWriter.write(sourceValue.toCSVRecord("SOURCE"), true);
        mismatchWriter.write(targetValue.toCSVRecord("TARGET"), true);
    }

    private void validateInputs(TransactionService source, TransactionService target, String outPath) {
        if (source == null || target == null) {
            throw new IllegalArgumentException("Transaction can't be null");
        }
        if (outPath == null) {
            throw new IllegalArgumentException("Output directory can't be null");
        }
    }

    private void validateOutputPath(String path) {
        DirectoryValidator.createPathIfNotExist(path);
    }

    private void prepareFiles() {
        this.matchingWriter.write("transaction id,amount,currecny code,value date", false);
        this.mismatchWriter.write("found in file,transaction id,amount,currecny code,value date", false);
        this.missingWriter.write("found in file,transaction id,amount,currency code,value date", false);
    }
}
