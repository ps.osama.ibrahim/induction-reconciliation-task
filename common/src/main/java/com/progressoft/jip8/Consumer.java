package com.progressoft.jip8;

import com.progressoft.jip8.FinancialTransaction;

public interface Consumer {
    void accept(FinancialTransaction transaction);
    boolean isEnd();
}
