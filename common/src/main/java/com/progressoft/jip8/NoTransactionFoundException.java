package com.progressoft.jip8;


//Ask if extending NullPointer exception makes more sense.
public class NoTransactionFoundException extends RuntimeException {
    public NoTransactionFoundException(String s) {
        super(s);
    }
}
