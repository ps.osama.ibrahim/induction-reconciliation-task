package com.progressoft.jip8;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public abstract class FileTransactionService implements TransactionService {

    private Map<String, FinancialTransaction> transactionMap = new HashMap<>();
    private boolean finish;
    private Path path;
    private Reader reader;

    public FileTransactionService(Reader reader) {
        this.reader = reader;
        finish = false;
    }

    @Override
    public FinancialTransaction findTransactionById(String id) {
        if (id == null)
            throw new NullPointerException("Transaction ID cant be null");
        FinancialTransaction financialTransaction = transactionMap.get(id);
        validateTransaction(id, financialTransaction);
        return financialTransaction;
    }

    @Override
    public void removeRecordById(String id) {
        if (id == null) throw new NullPointerException("Transaction ID cant be null");
        transactionMap.remove(id);
    }

    @Override
    public Map<String, FinancialTransaction> getAllRecords() {
        AllTransactionRecords transactions = new AllTransactionRecords();
        reader.read(transactions);
        if (transactions.map.size() > 0)
            return transactions.map;
        throw new NoTransactionFoundException("No transactions found in the CSV file");

    }

    private void validateTransaction(String id, FinancialTransaction transaction) {
        if (transaction == null)
            throw new NoTransactionFoundException(String.format("Unable to find transaction: %s in CSV file", id));
    }


    @Override
    public Map<String, FinancialTransaction> getRemainingRecords() {

        return new HashMap<>(transactionMap);
    }

    private class AllTransactionRecords implements Consumer {
        Map<String, FinancialTransaction> map = new HashMap<>();
        boolean finish = false;

        @Override
        public void accept(FinancialTransaction transaction) {
            if (transaction == null)
                throw new NullPointerException("Null Transaction");
            if (map.get(transaction.getTransactionID()) != null) {
                throw new IllegalStateException(
                        String.format("Duplicate entries in the file, transaction %s already exists", transaction.getTransactionID())
                );
            }
            map.put(transaction.getTransactionID(), transaction);
        }

        @Override
        public boolean isEnd() {
            return finish;
        }
    }
}
