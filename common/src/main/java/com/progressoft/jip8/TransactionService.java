package com.progressoft.jip8;

import java.util.Map;
public interface TransactionService {
    FinancialTransaction findTransactionById(String id);

    void removeRecordById(String id);

    Map<String, FinancialTransaction> getAllRecords();

    Map<String, FinancialTransaction> getRemainingRecords();

}
