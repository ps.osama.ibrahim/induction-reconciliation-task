package com.progressoft.jip8;

public interface Filewriter {
    void write(String content, boolean append);
}
