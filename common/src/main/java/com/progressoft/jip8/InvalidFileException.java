package com.progressoft.jip8;

public class InvalidFileException extends RuntimeException {
    public InvalidFileException(String msg){
        super(msg);
    }

    public InvalidFileException(String s, Exception e) {
        super(s, e);
    }
}
