import com.progressoft.jip8.AuthFilter;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;



public class AuthFilterTest {


    @Test
    public void givenInvalidSession_whenFilternig_redirect() throws ServletException, IOException {
        AuthFilter filter = new AuthFilter();
        HttpServletRequest mockReq = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse mockResp = Mockito.mock(HttpServletResponse.class);
        FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
        FilterConfig mockFilterConfig = Mockito.mock(FilterConfig.class);
        HttpSession session = Mockito.mock(HttpSession.class);
        Mockito.when(mockReq.getSession()).thenReturn(session);
        Mockito.when(mockReq.getRequestURI() )
                .thenReturn( "/reconciliation" );

        filter.init(mockFilterConfig);
        filter.doFilter(mockReq, mockResp, mockFilterChain);

        Mockito.verify(mockResp).sendRedirect("/login");
    }

    @Test
    public void givenValidSession_whenFilternig_Pass() throws ServletException, IOException {
        AuthFilter filter = new AuthFilter();
        HttpServletRequest mockReq = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse mockResp = Mockito.mock(HttpServletResponse.class);
        FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
        FilterConfig mockFilterConfig = Mockito.mock(FilterConfig.class);

        HttpSession session = Mockito.mock(HttpSession.class);
        session.setAttribute("username","osama");
        Mockito.when(mockReq.getSession()).thenReturn(session);


        Mockito.when( mockReq.getRequestURI() )
                .thenReturn( "/reconciliation" );


        filter.init(mockFilterConfig);
        filter.doFilter(mockReq, mockResp, mockFilterChain);

        //Mockito.verify(mockResp)
        //TEST PASS?!?!?
    }
}
