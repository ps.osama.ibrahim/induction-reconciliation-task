package com.progressoft.jip8;

import com.progressoft.jip8.web.LoginServlet;
import com.progressoft.jip8.web.ReconciliationServlet;
import com.progressoft.jip8.web.StaticFileServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class FragmentsInitializer implements  ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        LoginPolicy localLoginPolicy = new LocalLoginPolicy();
        SessionPolicy httpSessionPolicy = new HttpSessionPolicy();

        registerLoginServlet(ctx, localLoginPolicy,httpSessionPolicy);
        registerReconciliationServlet(ctx);
        registerStaticServlet(ctx);
    }

    private void registerReconciliationServlet(ServletContext ctx) {
        ReconciliationServlet reconciliationServlet = new ReconciliationServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("reconciliationServlet", reconciliationServlet);
        registration.addMapping("/reconciliation");
    }

    private void registerLoginServlet(ServletContext ctx, LoginPolicy loginPolicy, SessionPolicy sessionPolicy){
        LoginServlet loginServlet = new LoginServlet(loginPolicy,sessionPolicy) ;
        ServletRegistration.Dynamic registration = ctx.addServlet("loginServlet", loginServlet);
        registration.addMapping("/login");
    }
    private  void registerStaticServlet(ServletContext ctx){
        StaticFileServlet staticFileServlet = new StaticFileServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("staticFileServlet",staticFileServlet);
        registration.addMapping("/static");
    }

}
