package com.progressoft.jip8.web;

import com.progressoft.jip8.Comparitor;
import com.progressoft.jip8.TransactionService;
import com.progressoft.jip8.TransactionServiceFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class WebReconciliator {


    private HttpServletRequest request;
    private HttpServletResponse response;


    //Change these into environment variables
    private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 2;
    private static final int MAX_REQUEST_SIZE = 1024 * 1024;


    public WebReconciliator(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    // TODO shouldn't handle the response, lets the servlet handle the exceptions and successful responses - Done
    public void reconciliate() throws ServletException {
        // TODO too much long method - can't separate, one block
        // TODO enhance: use getPart instead - Can't make it work, reverted back

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MAX_MEMORY_SIZE);
        String resPath = createOutputDirectory();
        factory.setRepository(new File(resPath));

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(MAX_REQUEST_SIZE);
        Map<String, String> params = new HashMap<>();
        try {
            validateTempPath(resPath);
            List items = upload.parseRequest(request);
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (!item.isFormField()) {
                    String fileName = new File(item.getName()).getName();
                    String filePath = resPath + File.separator + fileName;
                    File uploadedFile = new File(filePath);
                    item.write(uploadedFile);
                    params.put(item.getFieldName(), filePath);
                } else {
                    params.put(item.getFieldName(), item.getString());
                }
            }
            request.getSession().setAttribute("tempFolder", resPath);
            startReconciliator(params,resPath);

        } catch (FileUploadException ex) {
            throw new ServletException(ex);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    private void validateTempPath(String uploadFolder) throws IOException {
        if (!Files.exists(Paths.get(uploadFolder))) {
            Files.createDirectories(Paths.get(uploadFolder));
        }
    }

    private void startReconciliator(Map<String, String> params, String path) {
        TransactionService sourceService = TransactionServiceFactory.
                getTransactionService(params.get("file1"), params.get("type1"));
        TransactionService targetService = TransactionServiceFactory.
                getTransactionService(params.get("file2"), params.get("type2"));
        Comparitor comparitor = new Comparitor(sourceService, targetService, path);
        comparitor.compare();
    }

    private String createOutputDirectory() {
        int randomNumber = (int) (Math.random() * 1000000000);
        String random = randomNumber + request.getSession().getId();
        String res = System.getProperty("user.home") + "/reconciliation-results/" + random;
        createPath(new File(res));
        return res;
    }

    private static void createPath(File file) {
        Path path = Paths.get(file.getPath());
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                throw new IllegalArgumentException("Directory can't be created");
            }
        }
    }
}
