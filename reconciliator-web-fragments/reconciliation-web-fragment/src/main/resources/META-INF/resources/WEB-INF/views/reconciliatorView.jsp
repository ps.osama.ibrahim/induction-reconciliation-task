
    <div class="container" id="contentArea">

<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
 <div id="wizardHeader">
        <form method="POST" enctype="multipart/form-data" id="filesForm">
        <div id="part1">
          <div class="alert alert-danger part1alert" role="alert">
                  This is a danger alert—check it out!
                </div>
        <div class="form-group">
             <label for="name1">Source name:</label>
             <input type="text" class="form-control" id="name1">
        </div>
             <div class="form-group">
              <select class="form-control" id="type1" name="type1">
                    <option value="csv">CSV</option>
                    <option value="json">JSON</option>
                </select>
             </div>
        <div class="form-group">
         <div class="custom-file">
               <input type="file"  name="file1" class="inpFileUpload custom-file-input"  accept=".json,.csv" id="file1">
               <label class="custom-file-label" for="customFile">Choose file 1</label>
             </div>
        </div>
        </div>

        <div id="part2">

        <div class="alert alert-danger part2alert" role="alert">
          This is a danger alert—check it out!
        </div>

            <div class="form-group">
              <label for="name2">Target name:</label>
              <input type="text" class="form-control" id="name2">
            </div>
            <div class="form-group">
                         <select class="form-control" id="type2" name="type2">
                               <option value="csv">CSV</option>
                               <option value="json">JSON</option>
                           </select>
              </div>
           <div class="form-group">
            <div class="custom-file">
                <input type="file"  name="file2" class=" inpFileUpload custom-file-input" accept=".json,.csv"  id="file2">
                <label class="custom-file-label" for="customFile2">Choose file 2</label>
            </div>
            </div>
        </div>


        <div id="part3">
           <div class="row">
             <div class="col-md-6">
              <div class="card" style="width:100%">
                <div class="card-header bg-primary text-white">
                 Source
                </div>
                <div class="card-body">
                  <p class="card-text">Name: <span id="spanName1"></span></p>
                  <p class="card-text">Type: <span id="spaType1"></span></p>
                </div>
              </div>


             </div>
             <div class="col-md-6">

              <div class="card">
                <div class="card-header bg-info text-white">
                  Target
                </div>
                <div class="card-body">
                   <p class="card-text">Name: <span id="spanName2"></span></p>
                   <p class="card-text">Type: <span id="spaType2"></span></p>
                </div>
              </div>

             </div>
           </div>
        </div>
      </div>
      <div id="wizardFooter">
        <button type="button" class="btn btn-primary btnNext" onclick="changeStep(this,1)">
          Next
        </button>
        <button type="button"  class="btn btn-primary btnPrevious" onclick="changeStep(this,-1)">
          Previous
        </button>
        <button type="button"  class="btn btn-primary btnSubmit" onclick="onSubmit()">
          Submit
        </button>
    </form>
    </div>
</div>
<div class="col-md-2"></div>
</div>


