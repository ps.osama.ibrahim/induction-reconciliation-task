package com.progressoft.jip8.web;


import com.progressoft.jip8.LoginPolicy;
import com.progressoft.jip8.SessionPolicy;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet  {

    LoginPolicy loginPolicy;
    SessionPolicy sessionPolicy;

    public LoginServlet(LoginPolicy loginPolicy, SessionPolicy sessionPolicy) {
        this.loginPolicy = loginPolicy;
        this.sessionPolicy = sessionPolicy;
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        checkSession(req, resp);
        // TODO your are still continuing the handling - Done
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
        dispatcher.forward(req, resp);
        return;
    }

    private void checkSession(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if(sessionPolicy.getAttribute(req,"username")!= null){
            resp.sendRedirect("/reconciliation");
            // TODO you should end the request handling - Done
            return;
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       boolean loggedIn =  loginPolicy.login(sessionPolicy,req);
       if(loggedIn){
           // TODO this attribute removal is done inside login policy - Done
           req.getSession().setAttribute("error",null);
           resp.sendRedirect("/reconciliation");
           return;
       }
       else {
           // TODO same as above  - Done
           req.getSession().setAttribute("error","Invalid Credentials");
           resp.sendRedirect("/login");
           return;
       }
    }

}
