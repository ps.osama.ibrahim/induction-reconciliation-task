<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4">

<c:if test="${sessionScope.error ne null}">
   <div class="alert alert-danger" role="alert">
     Invalid Credentials
   </div>
</c:if>

<form class="form-signin mt-5" method="post">
      <div class="form-label-group">
        <input class="form-control" type="text" name="username" required autofocus>
        <label for="inputEmail">Email address</label>
      </div>
      <div class="form-label-group">
        <input type="password" id="inputPassword" class="form-control" name="password" required>
        <label for="inputPassword">Password</label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</div>
<div class="col-md-4"></div>
</div>



