import com.progressoft.jip8.HttpSessionPolicy;
import com.progressoft.jip8.LocalLoginPolicy;
import com.progressoft.jip8.LoginPolicy;
import com.progressoft.jip8.SessionPolicy;
import com.progressoft.jip8.web.LoginServlet;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.mockito.Mockito.*;

// TODO where is the package
public class LoginServletTest {
    @Test
    @Disabled("session mock is missing")
    public void givenValidRequest_whenCalling_dispatcherCalled() throws IOException, ServletException {
        LoginPolicy loginPolicy = new LocalLoginPolicy();
        SessionPolicy sessionPolicy = new HttpSessionPolicy();
        // TODO test case failure because of session mocking - Done
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        HttpSession httpSession = mock(HttpSession.class);
        when(request.getSession()).thenReturn(httpSession);
        when(request.getRequestDispatcher("/WEB-INF/views/login.jsp")).thenReturn(requestDispatcher);
        new LoginServlet(loginPolicy, sessionPolicy).doGet(request, response);
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void givenValidCredentials_whenLoggingin_createSessionAndRedirect() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        LoginPolicy loginPolicy = new LocalLoginPolicy();
        SessionPolicy sessionPolicy = new HttpSessionPolicy();

        HttpSession session = mock(HttpSession.class);
        when(request.getParameter("username")).thenReturn("1");
        when(request.getParameter("password")).thenReturn("1");
        when(request.getSession()).thenReturn(session);
        new LoginServlet(loginPolicy, sessionPolicy).doPost(request, response);
        verify(request, atLeast(1)).getParameter("username");
        verify(request, atLeast(1)).getParameter("password");
        verify(session).setAttribute("username", "1");
    }

    @Test
    public void givenInvalidCredentials_whenLoggingin_createSessionAndRedirect() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        LoginPolicy loginPolicy = new LocalLoginPolicy();
        SessionPolicy sessionPolicy = new HttpSessionPolicy();

        HttpSession session = mock(HttpSession.class);
        when(request.getParameter("username")).thenReturn("2");
        when(request.getParameter("password")).thenReturn("2");
        when(request.getSession()).thenReturn(session);
        new LoginServlet(loginPolicy, sessionPolicy).doPost(request, response);
        verify(request, atLeast(1)).getParameter("username");
        verify(request, atLeast(1)).getParameter("password");
        verify(response).sendRedirect("/login");
    }
}

