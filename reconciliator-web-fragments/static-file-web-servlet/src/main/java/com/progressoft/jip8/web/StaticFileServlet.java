package com.progressoft.jip8.web;

import com.progressoft.jip8.CsvToJsonConverter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StaticFileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TODO need refactoring - Done
        String tempFolder = req.getSession().getAttribute("tempFolder").toString();
        String ext = req.getParameter("ext");
        String file = req.getParameter("file");
        String fullExt = getFileTypeFromFlag(ext);

        Path path = getPath(tempFolder, file, fullExt);
        Path csvPath = getPath(tempFolder, file, "csv");

        validateParams(tempFolder, file, csvPath, ext);

        if (fullExt.equals("json"))
            generateJsonFile(csvPath.toString());

        // TODO you don't need to do a reset - Done
        resp.setContentType("application/octet-stream");
        resp.setHeader("Content-Disposition", "attachment; filename=\"" + file + "." + fullExt + "\"");
        PrintWriter writer = resp.getWriter();
        try (BufferedReader br = Files.newBufferedReader(path)) {
            String line;
            while ((line = br.readLine()) != null) {
                writer.println(line);
            }
        }
    }

    private Path getPath(String tempFolder, String file, String fullExt) {
        return Paths.get(String.format("%s/%s", tempFolder, file + "." + fullExt));
    }

    // TODO misleading name - Done
    private void validateParams( String tempFolder, String file, Path path, String ext) throws IOException {
        if (file == null || tempFolder == null || ext == null) {
            throw new IllegalArgumentException("Invalid parameters, provide a file");
        }
        if (!Files.exists(path)) {
            throw new  FileNotFoundException("File Can't be found");
        }
    }

    private void generateJsonFile(String path) {
        CsvToJsonConverter csvToJsonConverter = new CsvToJsonConverter();
        csvToJsonConverter.convert(path);
    }
    private String getFileTypeFromFlag(String flag){
        for(FileTypeFlag c:FileTypeFlag.values()){
            if(c.name().equals(flag))
                return c.toString();
        }
        throw new IllegalArgumentException("Illegal File Type Flag");
    }
}
