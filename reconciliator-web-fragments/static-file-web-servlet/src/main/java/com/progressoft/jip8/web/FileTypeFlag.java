package com.progressoft.jip8.web;

public enum FileTypeFlag {
    j("json"), c("csv");

    private final String fullValue;

    FileTypeFlag(String value) {
        this.fullValue = value;
    }

    @Override
    public String toString() {
        return fullValue;
    }
}
