package com.progressoft.jip8;

public interface Transaction {
    String getTransactionID();
    String toCSVRecord();
    String toCSVRecord(String source);
    String toJSON();
    String toJSON(String source);
}
