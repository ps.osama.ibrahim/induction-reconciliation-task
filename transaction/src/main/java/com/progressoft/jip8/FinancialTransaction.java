package com.progressoft.jip8;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FinancialTransaction implements Transaction {
    // TO-DO you could keep the fields you need for comparision
    private String transactionID;
    private String currencyCode;
    private BigDecimal amount;
    private Date date;

    public String getTransactionID() {
        return transactionID;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }


    public Date getDate() {
        return date;
    }

    public FinancialTransaction(String transactionID,
                                String currencyCode,
                                BigDecimal amount,
                                Date date) {
        this.transactionID = transactionID;
        this.currencyCode = currencyCode;
        this.amount = amount.setScale(3);
        this.date = date;
    }

    public static FinancialTransaction generateTransaction
            (String transactionID, String currencyCode, String description, BigDecimal amount, String purpose, Date date, String transactionType) {
        return new FinancialTransaction(transactionID, currencyCode, amount, date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinancialTransaction targetFinancialTransaction = (FinancialTransaction) o;
        if (
                !this.transactionID.equalsIgnoreCase(targetFinancialTransaction.getTransactionID()) ||
                        !this.date.equals(targetFinancialTransaction.date) ||
                        !this.currencyCode.equalsIgnoreCase(targetFinancialTransaction.getCurrencyCode()) ||
                        this.getAmount().compareTo(targetFinancialTransaction.amount) != 0
        )
            return false;
        return true;
    }

    @Override
    public String toCSVRecord() {
        return String.format("%s,%s,%s,%s",
                getTransactionID(),
                getAmount(),
                getCurrencyCode(),
                dateFormatter(getDate())
        );
    }

    @Override
    public String toCSVRecord(String source) {
        return String.format("%s,%s", source, toCSVRecord());
    }

    @Override
    public String toJSON() {
        return String.format("{\"id\":\"%s\",\"amount\":\"%s\",\"currencyCode\":\"%s\",\"date\":\"%s\"}",
                getTransactionID(),
                getAmount(),
                getCurrencyCode(),
                dateFormatter(getDate())
        );
    }
    @Override
    public String toJSON(String source) {
        return String.format("{\"id\":\"%s\",\"foundInFile\":\"%s\",\"amount\":\"%s\",\"currencyCode\":\"%s\",\"date\":\"%s\"}",
                getTransactionID(),
                source,
                getAmount(),
                getCurrencyCode(),
                dateFormatter(getDate())
        );
    }

    private String dateFormatter(Date d) {
        return new SimpleDateFormat("yyyy-MM-dd").format(d);
    }
}
