package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FinancialTransactionTest {
    @Test
    public void givenFinancialTransaction_whenConvertingToCSV_returnValidCSV() throws ParseException {
        FinancialTransaction transaction = new FinancialTransaction("id",
                "code", BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));
        Assertions.assertEquals("id,1.000,code,2000-01-01", transaction.toCSVRecord());
    }

    @Test
    public void givenFinancialTransaction_whenConvertingToCSVWithSource_returnValidCSVWtihSource() throws ParseException {
        FinancialTransaction transaction = new FinancialTransaction("id",
                "code", BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));
        Assertions.assertEquals("SOURCE,id,1.000,code,2000-01-01", transaction.toCSVRecord("SOURCE"));
    }

    @Test
    public void givenFinancialTransaction_whenValidatingAttributes_returnTrue() throws ParseException {
        FinancialTransaction transaction = new FinancialTransaction("id",
                "code",
                BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));
        Assertions.assertEquals(transaction.getAmount(), BigDecimal.ONE.setScale(3));
        Assertions.assertEquals(transaction.getCurrencyCode(), "code");
        Assertions.assertEquals(transaction.getDate(), new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));
        Assertions.assertEquals(transaction.getTransactionID(), "id");
    }

    @Test
    public void givenTwoDifferentFinancialRecords_whenCompated_returnfalse() throws ParseException {
        FinancialTransaction transaction1 = new FinancialTransaction("id",
                "code",
                BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));

        FinancialTransaction transaction2 = new FinancialTransaction("id2",
                "code2",
                BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));
        Assertions.assertEquals(false, transaction1.equals(transaction2));
    }

    @Test
    public void givenTwoSameFinancialRecords_whenCompared_returnFalse() throws ParseException {
        FinancialTransaction transaction1 = new FinancialTransaction("id",
                "code",
                BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));

        FinancialTransaction transaction2 = new FinancialTransaction("id",
                "code",
                BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));
        Assertions.assertEquals(true, transaction1.equals(transaction2));
    }

    @Test
    public void givenInvalidDate_whenCreatingTransaction_throwException() throws ParseException {
        Assertions.assertThrows(ParseException.class, () ->
                new FinancialTransaction(
                        "id",
                        "code",
                        BigDecimal.ONE,
                        new SimpleDateFormat("yyyy-MM-dd").parse("2000/01/01"))
        );
    }

    @Test
    public void givenValidCredentials_whenCreatingUsingStaticmethod_returnValidMethod() throws ParseException {
        FinancialTransaction transaction = new FinancialTransaction(
                "id",
                "code",
                BigDecimal.ONE,
                new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));

        Assertions.assertEquals(transaction,
                FinancialTransaction.generateTransaction("id", "code", "description",
                        BigDecimal.ONE, "purpose",
                        new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"), "type")
        );
    }
}
