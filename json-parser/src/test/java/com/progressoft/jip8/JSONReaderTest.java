package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class JSONReaderTest {

    @Test
    public void givenAValidCSVFile_WhenReading_ReturnValidTransactionMap() throws ParseException {
        Map<String,FinancialTransaction> map = new HashMap<>();
        JSONTransactionServiceProvider service = new JSONTransactionServiceProvider(
                Paths.get("src/test/resources/test.json")
        );
        map.put("TR-47884222201",
                new FinancialTransaction(
                        "TR-47884222201",
                        "USD",
                        new BigDecimal("140.00"),
                        new SimpleDateFormat("dd/MM/yyyy").parse("20/01/2020")
                ));
        Assertions.assertEquals(map,service.getAllRecords());
    }

    @Test
    public void givenAValidJSONFile_WhenReading_ReturnValidTransactionMapCount() throws ParseException{
        JSONTransactionServiceProvider service = new JSONTransactionServiceProvider( Paths.get("src/test/resources/test.json"));
        Assertions.assertEquals(1,service.getAllRecords().size());
    }
    @Test
    public void givenAnInvalidJSONFile_WhenReading_throwException() throws ParseException{
        JSONTransactionServiceProvider service = new JSONTransactionServiceProvider( Paths.get("src/test/resources/testDefective.json"));
        Assertions.assertEquals(1,service.getAllRecords().size());
    }



}
