package com.progressoft.jip8;

import java.nio.file.Path;

public class JSONTransactionServiceProvider extends FileTransactionService {

    public JSONTransactionServiceProvider(Path path) {
        super(new JSONReader(path));
    }

}
