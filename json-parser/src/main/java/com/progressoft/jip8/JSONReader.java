package com.progressoft.jip8;

import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;


public class JSONReader implements Reader {

    private Path path;

    public static final String PURPOSE = "purpose";
    public static final String AMOUNT = "amount";
    public static final String REFERENCE = "reference";
    public static final String DATE = "date";
    public static final String CURRENCY_CODE = "currencyCode";


    public JSONReader(Path path) {
        validatePath(path);
        this.path = path;
    }


    //pass down consumer as callback, reference induction videos and currency converter project
    //mimics javascript callback behavior
    @Override
    public void read(Consumer consumer) {
        Object obj = null;
        try {
            obj = new JSONParser().parse(new FileReader(path.toString()));
            JSONArray jsonArray = (JSONArray) obj;
            readProcess(consumer, jsonArray);
        } catch (Exception e) {
            throw new InvalidFileException("Json file Corrupted", e);
        }

    }

    private void readProcess(Consumer consumer, JSONArray jsonArray) {
        Iterator itterator = jsonArray.iterator();
        while (itterator.hasNext()) {
            JSONObject jsonObject = (JSONObject) itterator.next();
            consumer.accept(generateFinancialTransaction(jsonObject));
        }

    }

    private FinancialTransaction generateFinancialTransaction(JSONObject obj) {
        return FinancialTransaction.generateTransaction(
                obj.get(REFERENCE).toString(),
                obj.get(CURRENCY_CODE).toString(),
                null,
                new BigDecimal(obj.get(AMOUNT).toString()),
                obj.get(PURPOSE).toString(),
                parseStringToDate(obj.get(DATE).toString()),
                null);
    }


    private void validatePath(Path path) {
        if (path == null) {
            throw new NullPointerException("Path can not be null");
        }
        if (Files.notExists(path)) {
            throw new InvalidFileException("File does not exist");
        }
        if (Files.isDirectory(path)) {
            throw new InvalidFileException("Path does not appear to be a file");
        }
        if (!getExtension(path).equalsIgnoreCase("json")) {
            throw new InvalidFileException("File must be a .json file");
        }
    }


    private Date parseStringToDate(String strDate) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
        } catch (ParseException e) {
            throw new InvalidFileException("Corrupt file", e);
        }
    }

    //https://stackoverflow.com/questions/3571223/how-do-i-get-the-file-extension-of-a-file-in-java
    private String getExtension(Path file) {
        return FilenameUtils.getExtension(file.toString());
    }
}
