package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVFileWriterTest {
    @Test
    public void givenValidPathAndContent_whenWriting_writestoFile() throws IOException {
        String content = "TEST";
        Path output = Files.createTempDirectory("output");
        // TODO absolute path!!! - Done
        CSVFileWriter writer = new CSVFileWriter(output.toString(), "test.csv");
        writer.write(content, false);
        try (BufferedReader reader = Files.newBufferedReader(output.resolve("test.csv"))) {
            Assertions.assertEquals(reader.readLine(), content);
        }
    }

    @Test
    public void givenInvalidPathAndContent_whenWriting_writestoFile() throws IOException {
        String content = "TEST";
        CSVFileWriter writer = new CSVFileWriter("/does/not/exist", "test.csv");
        Exception e = Assertions.assertThrows(FileWriterException.class, () ->
                writer.write(content, false));
        Assertions.assertEquals("Unable to write to file /does/not/exist/test.csv\n" +
                "java.io.FileNotFoundException: /does/not/exist/test.csv (No such file or directory)", e.getMessage());
    }


}
