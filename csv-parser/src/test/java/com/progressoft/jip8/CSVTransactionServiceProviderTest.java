package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class CSVTransactionServiceProviderTest {

    @Test
    public void givenAValidCSVFile_WhenReading_ReturnValidTransactionMap() throws ParseException {
        Map<String, FinancialTransaction> map = new HashMap<>();
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        map.put("TR-47884222201",
                new FinancialTransaction("TR-47884222201",
                        "USD", new BigDecimal("140"),
                        new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-20")
                ));
        Assertions.assertEquals(map, service.getAllRecords());
    }


    @Test
    public void givenAValidCSVFile_WhenReading_ReturnValidTransactionMapCount() {
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        Assertions.assertEquals(1, service.getAllRecords().size());
    }



    @Test
    public void givenNULLRecordID_whenRemoving_throwException() {
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        Exception e = Assertions.assertThrows(NullPointerException.class, () -> service.removeRecordById(null));
        Assertions.assertEquals("Transaction ID cant be null", e.getMessage());
    }

    @Test
    public void givenRecordID_whenRemoving_Passes() {
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        service.removeRecordById("TR-47884222201");
    }

    @Test
    public void givenNullRecordID_whenFinding_throwException() {
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        Exception e = Assertions.assertThrows(NullPointerException.class, () ->
                service.findTransactionById(null));
        Assertions.assertEquals("Transaction ID cant be null", e.getMessage());
    }

    @Test
    public void givenNullRecordID_whenRemoving_throwException() {
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        Exception e = Assertions.assertThrows(NullPointerException.class, () ->
                service.removeRecordById(null));
        Assertions.assertEquals("Transaction ID cant be null", e.getMessage());
    }

    @Test
    public void givenValidRecordID_whenRemoving_Pass() {
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        service.removeRecordById("TR-47884222201");
    }

    @Test
    public void givenValidCSVFile_whenFindingANonExistantID_throwException() {
        CSVTransactionServiceProvider service = new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.csv"));
        Exception e = Assertions.assertThrows(NoTransactionFoundException.class, () ->
                service.findTransactionById("123"));
        Assertions.assertEquals("Unable to find transaction: 123 in CSV file", e.getMessage());
    }

}
