package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class CSVReaderTest {
    @Test
    public void givenAnInvalidFile_whenReading_ThrowException() {
        Exception e = Assertions.assertThrows(InvalidFileException.class, () ->
                new CSVTransactionServiceProvider(Paths.get("src/test/resources/test.json"))
        );
        Assertions.assertEquals(e.getMessage(),"File must be a .csv file");
    }
    @Test
    public void givenNullPath_whenCreating_ThrowException(){
        Exception e = Assertions.assertThrows(NullPointerException.class, ()->
                new CSVReader(null));
        Assertions.assertEquals(e.getMessage(),"Path can not be null");
    }
    @Test
    public void givenPathThatDoesntExist_whenCreating_ThrowException(){
        Exception e =  Assertions.assertThrows(InvalidFileException.class, ()->
                new CSVReader(Paths.get("/doesnt/exist/on/this/machine")));
        Assertions.assertEquals(e.getMessage(),"File does not exist");
    }
    @Test
    public void givenPathThatIsADiretory_whenCreating_ThrowException(){
        Exception e =   Assertions.assertThrows(InvalidFileException.class, ()->
                new CSVReader(Paths.get("/home/")));
        Assertions.assertEquals(e.getMessage(),"Path does not appear to be a file");
    }
}
