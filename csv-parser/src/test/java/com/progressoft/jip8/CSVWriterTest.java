package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CSVWriterTest {
    @Test
    public void givenContent_whenWriting_writesToFile() throws IOException {
        CSVFileWriter writer = new CSVFileWriter("src/test/resources","writerTest.csv");
        writer.write("TEST",false);
        try (BufferedReader reader = Files.newBufferedReader(Paths.get("src/test/resources/writerTest.csv"))) {
            Assertions.assertEquals(reader.readLine(),"TEST");
        }

    }
}
