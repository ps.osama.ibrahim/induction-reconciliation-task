package com.progressoft.jip8;

import java.io.*;

public class CSVFileWriter implements Filewriter {
    private String path;
    private String fileName;

    public CSVFileWriter(String path, String fileName) {
        this.path = path;
        this.fileName = fileName;
    }

    @Override
    public void write(String content, boolean append) {
        try (OutputStream os = new FileOutputStream(new File(path + "/" + fileName), append)) {
            os.write((content + "\n").getBytes());
            os.flush();
        } catch (Exception e) {
            throw new FileWriterException("Unable to write to file " + path + "/" + fileName + "\n" + e);
        }
    }
}
