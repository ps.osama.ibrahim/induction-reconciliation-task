package com.progressoft.jip8;

import java.nio.file.Path;

public class CSVTransactionServiceProvider extends FileTransactionService {

    public CSVTransactionServiceProvider(Path path) {
        super(new CSVReader(path));
    }

}
