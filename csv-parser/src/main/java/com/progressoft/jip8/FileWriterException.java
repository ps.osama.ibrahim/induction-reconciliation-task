package com.progressoft.jip8;

public class FileWriterException extends RuntimeException {
    public FileWriterException(String s) {
        super(s);
    }

}
