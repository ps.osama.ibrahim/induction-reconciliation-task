package com.progressoft.jip8;

import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CSVReader implements Reader {

    private Path path;

    public CSVReader(Path path) {
        validatePath(path);
        this.path = path;
    }

    private void validatePath(Path path) {
        if (path == null) {
            throw new NullPointerException("Path can not be null");
        }
        if (Files.notExists(path)) {
            throw new InvalidFileException("File does not exist");
        }
        if (Files.isDirectory(path)) {
            throw new InvalidFileException("Path does not appear to be a file");
        }
        if (!getExtension(path).equalsIgnoreCase("csv")) {
            throw new InvalidFileException("File must be a .csv file");
        }
    }

    @Override
    public void read(Consumer consumer) {
        try (BufferedReader reader = Files.newBufferedReader(this.path)) {
            skipHeader(reader);
            readProcess(consumer, reader);
        } catch (IOException e) {
            throw new InvalidFileException("Corrupt file", e);
        }
    }

    private void readProcess(Consumer consumer, BufferedReader reader) {
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = splitRecordIntoFields(line);
                consumer.accept(lineToFinancialTransaction(fields));
            }
        } catch (IOException e) {
            throw new InvalidFileException("Corrupt file", e);
        }

    }

    private FinancialTransaction lineToFinancialTransaction(String[] fields) {
        String id = fields[0];
        BigDecimal amount = new BigDecimal(fields[2]);
        String currencyCode = fields[3];
        Date date = parseStringToDate(fields[5]);
        return FinancialTransaction.generateTransaction(id, currencyCode, null, amount, null, date, null);
    }

    private Date parseStringToDate(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
        } catch (ParseException e) {
            throw new InvalidFileException("Corrupt file", e);
        }
    }

    private void skipHeader(BufferedReader bufferedReader) {
        try {
            bufferedReader.readLine();
        } catch (IOException e) {
            throw new InvalidFileException("Corrupt file", e);
        }
    }

    private String[] splitRecordIntoFields(String record) {
        String[] result = record.split(",");
        if(result.length == 1){
            throw new NoTransactionFoundException("Corrupt CSV File");
        }
        return result;
    }

    private String getExtension(Path file) {
        return FilenameUtils.getExtension(file.toString());
    }


}
