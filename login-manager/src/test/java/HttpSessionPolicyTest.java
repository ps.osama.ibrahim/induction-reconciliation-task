import com.progressoft.jip8.HttpSessionPolicy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


//TEST THIS
public class HttpSessionPolicyTest {
    HttpServletRequest mockReq ;
    HttpServletResponse mockResp ;
    FilterChain mockFilterChain;
    FilterConfig mockFilterConfig;
    HttpSession session;
    HttpSessionPolicy policy;

    @BeforeEach
    public void init() {
        mockReq = Mockito.mock(HttpServletRequest.class);
        mockResp = Mockito.mock(HttpServletResponse.class);
        mockFilterChain = Mockito.mock(FilterChain.class);
        mockFilterConfig = Mockito.mock(FilterConfig.class);
        session = Mockito.mock(HttpSession.class);
        Mockito.when(mockReq.getSession()).thenReturn(session);
        policy = new HttpSessionPolicy();
    }

//    @Test
//    public void givenValidAttributes_whenSettingSessionVariable_Pass(){
//
//        policy.setAttribute(mockReq,"TEST","TEST");
//        Assertions.assertEquals("TEST",session.getAttribute("TEST"));
//    }
//    @Test
//    public void giveValidAttributeName_whenGettingSessionVariable_Pass(){
//        policy.setAttribute(mockReq,"TEST","TEST");
//        Assertions.assertEquals("TEST",session.getAttribute("TEST"));
//    }
    @Test
    public void giveValidSession_whenDestroying_destroySession(){
        policy.destroySession(mockReq);
        Assertions.assertEquals(null,session.getAttributeNames());
    }
}
