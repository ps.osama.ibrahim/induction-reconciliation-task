package com.progressoft.jip8;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface LoginPolicy {
    boolean login(SessionPolicy sessionPolicy , HttpServletRequest request) throws IOException;
    void logout(HttpServletRequest request);
}
