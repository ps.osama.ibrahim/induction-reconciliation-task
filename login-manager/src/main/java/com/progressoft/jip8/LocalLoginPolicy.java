package com.progressoft.jip8;


import javax.servlet.http.HttpServletRequest;

import java.io.IOException;

public class LocalLoginPolicy implements LoginPolicy {


    @Override
    public void logout(HttpServletRequest request) {
        request.getSession().invalidate();
    }

    @Override
    public boolean login(SessionPolicy sessionPolicy, HttpServletRequest request) throws IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        validateLoginParams(username, password);

        //Inject Data source
        if (username.equals("1") && password.equals("1")) {
            sessionPolicy.setAttribute(request,"username", username);
            sessionPolicy.removeAttribute(request,"error");
            return true;
        }
        sessionPolicy.setAttribute(request,"error", "Invalid username or password");
        return false;
    }

    private void validateLoginParams(String username, String password) {
        if (username == null || username.isEmpty()) {
            throw new IllegalArgumentException("Username can not be null or empty");
        }
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Password can not be null or empty");
        }
    }



}
