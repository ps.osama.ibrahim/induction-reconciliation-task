package com.progressoft.jip8;


import javax.servlet.http.HttpServletRequest;

public interface SessionPolicy {
    void setAttribute(HttpServletRequest request, String name, Object value);
    Object getAttribute(HttpServletRequest request, String name);
    void removeAttribute(HttpServletRequest request, String name);
    void destroySession(HttpServletRequest request);
}
