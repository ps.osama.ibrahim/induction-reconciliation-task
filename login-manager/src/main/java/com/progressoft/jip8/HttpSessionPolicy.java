package com.progressoft.jip8;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class HttpSessionPolicy implements SessionPolicy {

    @Override
    public void setAttribute( HttpServletRequest request, String name, Object value) {
        validateSetAttributesParams(request,name,value);
        HttpSession session = request.getSession();
        session.setAttribute(name, value);
    }

    @Override
    public Object getAttribute(HttpServletRequest request, String name) {
        validateGetAttributeParams(request,name);
        HttpSession session = request.getSession();
        return session.getAttribute(name);
    }

    @Override
    public void removeAttribute(HttpServletRequest request, String name) {
        validateRemoveAttributeParams(name);
        request.removeAttribute(name);
    }

    @Override
    public void destroySession(HttpServletRequest request) {
        request.getSession().invalidate();
    }

    private void validateRemoveAttributeParams(String name) {
        if(name==null || name.isEmpty()){
            throw new  IllegalArgumentException("Can not remove session attribute, Invalid session attribute name");
        }
    }

    private void validateSetAttributesParams(HttpServletRequest request, String name, Object value){
        if(request==null)
            throw new IllegalArgumentException("set attribute request can not be null");
        if(name==null)
            throw new IllegalArgumentException("set attribute name can not be null");
        if(value==null)
            throw new IllegalArgumentException("set attribute value can not be null");
    }

    private void validateGetAttributeParams(HttpServletRequest request, String name){
        if(request==null)
            throw new IllegalArgumentException("set attribute request can not be null");
        if(name==null)
            throw new IllegalArgumentException("set attribute name can not be null");

    }
}
